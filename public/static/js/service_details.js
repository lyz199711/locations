var url =window.location.pathname;   //获取当前页面的url
    var urlParam = window.location.search.substr(1);//页面参数   
    var arr = urlParam.split("="); //将参数按照&符分成数组  

    var port = "";
    switch (arr[1]) {
        case "lawyer":
            //律师服务
            port = "/service/lawyer_service"
            break;
        case "accounting":
            //会计服务
            port = "/service/accounting_service"
            break;
        case "registration":
            //注册服务
            port = "/service/registration_service"
            break;
        case "financing":
            //投资融资服务
            port = "/service/investment_financing"
            break;
        case "resources":
            //资源对接服务
            port = "/service/resources_service"
            break;
        case "business":
            //商务洽谈服务
            port = "/service/business_service"
            break;
        case "advisory":
            //顾问咨询服务
            port = "/service/advisory_service"
            break;

    }
    

    $.ajax({
        type: "GET",
        url: port,
        async: true,//请求是否异步，默认为异步，这也是ajax重要特性
        dataType: "json",
        success: function (data) {
            console.log(data);
            $(".details").empty();
            if (data != "") {
                

                 var time = new Date(data.create_time).toLocaleDateString().replace(/\//g, '-')
                
                var lihtml = $(
                    ` 
                   
                    <div class="lawyer_details_box1 col-xs-12">
                        <div class="lawyer_details_box2 col-xs-12">
                            <ul>
                                <li><a href="/index">首页</a> &ensp;>></li>
                                <li><a href="javascript:history.back(-1)">顾问服务</a> &ensp;>></li>
                                <li><a href="javascript:location.reload();">服务详情</a> &ensp;>></li>
                            </ul>
                        </div>

                        <div class="lawyer_details_box3 col-xs-12">
                            <h3>${data.article_title}</h3>
                        </div>
                        <div class="lawyer_details_box4 col-xs-12">
                            <p>上传日期:${time} 浏览次数: ${data.visits}</p>
                        </div>
                        <div class="lawyer_details_box5 col-xs-12">
                            <p>${data.detail}</p>
                        </div>
                        <div class="lawyer_details_box6 col-xs-12">
                            <img src="/static/images/pc/6_image06.png" alt="" class="img-responsive">
                        </div>


                    </div>      
                            `)

                $(".details").html(lihtml);

              

            }


        }
    })