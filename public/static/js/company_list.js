
//中英文
var language = $('#language').attr('value');


//判断是否查询全部
var urlParamArr = window.location.search.substr(1);//页面参数  

// if (urlParamArr == "") {
//     company_list();
// } else {
    var arr = urlParamArr.split("&"); //将参数按照&符分成数组
    company_search(arr)

$('.state1').selectpicker({});


//选择国家触发
$('#slpk1').on('changed.bs.select', function (e) {
    var country_id = $('#slpk1 option:selected').val();
    // alert(country_id)
    var url = window.location.pathname;   //获取当前页面的url
    var urlParam = window.location.search.substr(1);//页面参数   
    var arr = urlParam.split("&"); //将参数按照&符分成数组
    
    
    for(var i=0;i <arr.length;i++){
        if((arr[i].split("="))[0] == "country_id"){
           
            arr.splice((arr.indexOf(arr[i])), 1 );
        }
    }
    arr.push(country_id); 
    
    var nextUrl = "";  
    if (arr.length > 0) {
        nextUrl = "?" + arr.join("&");
        
    }
    url = url + nextUrl;

    history.pushState({}, '', url);
    company_search(arr)
    })
  

   
    


//分页
function page(obj, page){

    // alert(page)
    var url = window.location.pathname;   //获取当前页面的url
    var urlParam = window.location.search.substr(1);//页面参数  
     var page = "page="+page
  $("#pagination")
    // alert(newurlParam)
    var arr = urlParam.split("&"); //将参数按照&符分成数组
    for(var i=0;i <arr.length;i++){
        if((arr[i].split("="))[0] == "page"){
           
            arr.splice((arr.indexOf(arr[i])), 1 );
        }
    }
    arr.push(page); 
    
    var nextUrl = "";  
    if (arr.length > 0) {
        nextUrl = "?" + arr.join("&");
        
    }
    // alert(arr)
    // alert(nextUrl)
    url = url + nextUrl;
    history.pushState({}, '', url);
    // alert(arr)
    company_search(arr)
}


//添加条件
function add(a, id) {
    
    // alert(1)
    var url = window.location.pathname;   //获取当前页面的url
    var urlParam = window.location.search.substr(1);//页面参数      
   
    // var beforeUrl = url.substr(0, url.indexOf("?"));   //页面主地址（参数之前地址）
    
    var nextUrl = "";
    
    var arr = new Array();
       

    if (urlParam != "" ) {      //判断是否存在参数 
        var arr = urlParam.split("&"); //将参数按照&符分成数组
         
        var count = 0;
       
       
        $(".u2").find("a").each(function () {
            if ($(this).hasClass("active")) {
                count++
            }
        });
        var idAll =  id.split("=");
  
       
        if (arr.indexOf(id) == -1 && count < 3 ) {
            arr.push(id);

            $(a).addClass("active");
        } else if(arr.indexOf(id) == -1 && idAll[0] == "business_type"){
            arr.push(id);
            $(a).addClass("active");
        } else if(arr.indexOf(id) == -1 &&  idAll[0] == "country_id" ){
            arr.push(id);
            
        } else if(arr.indexOf(id) != -1 ){
            arr.splice((arr.indexOf(id)), 1);
            $(a).removeClass("active");


        }


    } else {
        arr.push(id);
        $(a).addClass("active");
       
    }

    if (arr.length > 0) {
        nextUrl = "?" + arr.join("&");

    }
    //    alert(arr)

    if (nextUrl == "") {
        
        company_search(arr)
    } else {
        company_search(arr)
        // return url;

    }
    url = url + nextUrl;

    history.pushState({}, '', url);
}


//条件查询
function company_search(arr) {
  
    var text = ""
    var industry_type = ""
    var business_type = ""
    var country_id = ""
    var page = ""

    var $industry_sel = ""
    var $business_sel = ""
    var $country_id = ""

    if(arr != ""){
        for (var i = 0; i < arr.length; i++) {
            var paramArr = arr[i].split("="); //将参数键，值拆开
            // console.log(paramArr)
            switch (paramArr[0]) {
                case "text":
                    //解决中文
                    text = decodeURI(paramArr[1]);
    
                    break;
                case "industry_type":
                    $industry_sel = "#industry_type" + paramArr[1]
                    // alert($industry_sel)
                    $($industry_sel).addClass('active')
                    industry_type += paramArr[1] + ","
                    break;
                case "business_type":
                    $business_sel = "#business_type" + paramArr[1]
                    $($business_sel).addClass('active')
                    business_type += paramArr[1] + ','
                    break;
                case "country_id":
                    $country_id = "#country_id" + paramArr[1]
                    $($country_id).attr('selected',"selected")
                   
                        country_id = paramArr[1]
                    
                    break;
                case "page":
                  
                        page = paramArr[1]
                    
                    break;
    
    
            }
        }

    }

    

  

    $.ajax({
        type: "GET",
        url: "/index/Company/highSearchCompany",
        async: true,//请求是否异步，默认为异步，这也是ajax重要特性
        data: {
            text: text,
            industry_type: industry_type,
            business_type: business_type,
            country_id: country_id,
            page: page
        },    //参数值
        dataType: "json",
        success: function (data) {
           
            
            var data = data.data.companyList
            
            // console.log(language)
            var country = "";
            if (data.data != "") {

                $(".cal").empty();

                if(language == "cn"){
                    var founding_time = "成立时间"
                    var company_type = "公司类型"
                    var industry_type = "行业类型"
                    var view_details = "查看详情"
                    }else{
                     var founding_time = "Founding Time"
                     var company_type = "Company Type"
                     var industry_type = "Industry Type"
                     var view_details = "View Details"
                    }

                $.each(data.data, function (i, item) {
                    if (item.region_id == null | item.region_id == 0) {
                        country = item.country_id;
                    } else {
                        country = item.country_id + " | " + item.region_id;
                    }

                    var lihtml = $(
                        `
                       <div class=" col-xs-12 col-sm-6 col-md-4 ">
                        <div class="company_list1">
                            <div class="company_list2">
                            <div class="company_list2_box1">
                                <ul style="list-style: none;">
                                    <li><p style="width:100%; white-space:nowrap; text-overflow:ellipsis; -o-text-overflow:ellipsis; overflow:hidden;font-size:18px;color:#000000" ><b><img src="/static/images/形状 3.png" alt="">&ensp;&ensp; ${item.name}</b></p></li>
                                    <li style="margin-top:10px;margin-bottom:10px;"><img src="/static/images/形状 2.png" alt="">&ensp;&ensp;<span>${country}</span></li>
                                    <li><span>${founding_time}: ${item.life_year}年</span></li>
                                    <li><span>${company_type}: ${item.business_type}</span></li>
                                    <li><p style="width:100%; white-space:nowrap; text-overflow:ellipsis; -o-text-overflow:ellipsis; overflow:hidden;" ><span>${industry_type}: ${item.industry_type}</span></p></li>
                                </ul>
                                </div >
                                <hr class="" style="border-top:2px dashed #e2e0e0 ; margin-top: 0px;">
                                <div>
                                    <h4 style="float: left;color: red;width:56%"><b> <p style="width:100%; white-space:nowrap; text-overflow:ellipsis; -o-text-overflow:ellipsis; overflow:hidden;">${item.price}</p></b></h4>
                                    <a href="/company_details?id=${item.language_union}"  style="float: right;">${view_details}</a>
                                </div>
                            </div> 

                        </div>

                        </div>
                       `)
                    $(".cal").append(lihtml);

                })
                
               
      
                    


            } else {
                
                $(".cal").empty();
                
                if(language == "cn"){
                    var lihtml = $(
                        `
                       <div col-xs-12 col-sm-6 col-md-4 " style="text-align:center;margin-top:130px;font-size:24px;margin-bottom:130px">
                            <p>暂无内容</p>
                       </div>
                       `)
                   
                    }else{
                        var lihtml = $(
                            `
                           <div col-xs-12 col-sm-6 col-md-4 " style="text-align:center;margin-top:130px;font-size:24px;margin-bottom:130px">
                                <p>No content</p>
                           </div>
                           `)
                   
                    }

 
                $(".cal").append(lihtml);
            }
            if(data.total != "0"){
                    
                P.initMathod({
                    params: {elemId: '#Page',total:data.total,pageIndex:data.current_page,pageNum:'5',pageSize:data.per_page},
                    requestFunction: function () {
                      
                        
                       JSON.stringify(P.config);
                    }
                });
            }else{
                $('#Page').empty();
            }

        }
    })
}




function state() {

    $.ajax({
        type: "GET",
        url: "index/Company/getCompanyListAll",
        async: true,//请求是否异步，默认为异步，这也是ajax重要特性
        dataType: "json",
        success: function (data) {

            // $(".state").empty();
            var data = data.data.country

            if (data.length != 0) {
                
                //循环后台的数据拼接上去

                $.each(data, function (i, item) {


                    var lihtml1 = $(
                        `
                    <option value="sid=${item.id}">${item.name}</option>                       
                    `
                    );

                    $(".state").append(lihtml1);

                })
            }



        }
    })
}


