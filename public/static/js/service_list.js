var urlParam = window.location.search.substr(1);//页面参数   
  var arr = urlParam.split("="); //将参数按照&符分成数组  
  
  var port = "";
  switch (arr[1]) {
    case "lawyer":
      //律师服务
      port = "/service/lawyer_service"
      break;
    case "accounting":
      //会计服务
      port = "/service/accounting_service"
      break;
    case "registration":
      //注册服务
      port = "/service/registration_service"
      break;
    case "financing":
      //投资融资服务
      port = "/service/investment_financing"
      break;
    case "resources":
      //资源对接服务
      port = "/service/resources_service"
      break;
    case "business":
      //商务洽谈服务
      port = "/service/business_service"
      break;
    case "advisory":
      //顾问咨询服务
      port = "/service/advisory_service"
      break;

  }
  
  $.ajax({
    type: "GET",
    url: port,
    async: true,//请求是否异步，默认为异步，这也是ajax重要特性
    dataType: "json",
    success: function (data) {
      // console.log(data);
      $(".service").empty();
      if (data != "") {
        //循环后台的数据拼接上去


        //    $.each(data, function (i, item) {

        var lihtml = $(
          ` 
                   
          <div class="col-xs-12 ">
            <div class="lawyer_service_box1 col-xs-12 ">
              <h3>${data.article_title}</h3>
            </div>

            <div class="lawyer_service_box2 col-xs-12">

              <div class="media  col-xs-12 ">
                <div class="media-left col-xs-12 col-sm-6 col-md-6">
                  <a href="#">
                    <img class="media-object img-responsive" src="/static/images/pc/6_image01.png" alt="...">
                  </a>
                </div>
                <div class="media-body ">
                  <h3 class="media-heading">${data.article_title}</h3>
                  <div class="lawyer_service_box3">
                    <p>${data.detail}</p>
                  </div>

                  <div class="lawyer_service_box4">

                    <a href="/service/service_details?${urlParam}">查看详情</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="lawyer_service_box5 col-xs-12">
              <hr>
            </div>
          </div>        
                            `)

        $(".service").html(lihtml);

        // })

      }


    }
  })