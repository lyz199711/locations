
//信息弹窗
function success_prompt(message,style,time){
   
    style = (style ===undefined) ?'alert-success': style; 

     time = (time ===undefined) ?1500: time; 
    $('<div>').appendTo('body').addClass('alert ' + style).html(message).show().delay(time).fadeOut();
    
}

// 成功提示varsuccess_prompt =function(message, time){    prompt(message,'alert-success', time);};

// 失败提示varfail_prompt =function(message, time){    prompt(message,'alert-danger', time);};

// 提醒varwarning_prompt =function(message, time){    prompt(message,'alert-warning', time);};

// 信息提示varinfo_prompt =function(message, time){    prompt(message,'alert-info', time);};