

//中英文
var language = $('#language').attr('value');


// function collect(id) {


// }
// $(".collect-icon").on("click", function (e) {


//     $.ajax({
//         url: "/company_collection",
//         type: "post",
//         data: {

//             company_id: "<?php echo $data['companyInfo']['id']; ?>"//项目ID
//         },
//         success: function (data) {
//             $('.collect-icon').css({ "color": "red" })
//         }
//     })

// })

//轮播图

var swiper = new Swiper('.swiper-container', {
    pagination: {
        el: '.swiper-pagination',
    },
});

var mySwiper = new Swiper('.swiper-container', {
    //   autoplay:true,//等同于以下设置
    autoplay: {
        delay: 3000,
        stopOnLastSlide: false,
        disableOnInteraction: true,
    },
});



function price(type) {
    $(".buy_3").find("*").removeClass("active");
    $(".buy_3").find("a").eq((type - 1)).addClass("active");

}


//收藏
function collect1(type) {
    $.ajax({
        type: "GET",
        url: "index/Company/collectionCompany",
        data: { company_id: type },
        async: "true",
        dataType: "json",
        success: function (data) {
            // console.log(data)
            // alert(data.msg)

            $('.collect2').show()
            $('.collect1').hide()

        }
    })
}




function collect2(type) {
    //取消收藏
    // alert(2)
    $.ajax({
        type: "get",
        url: "/index/Company/deleteCollection",
        data: {
           
            company_id: type
        },
        async: "true",
        dataType: "json",
        success: function (data) {

            // console.log(data)
            if (data == '1') {

                $('.collect2').hide()
                $('.collect1').show()
            }




        }
    })
}

details();

//公司详情
function details() {
    var urlParam = window.location.search.substr(1);//页面参数   
    var urlParamarr = urlParam.split("="); //将参数按照&符分成数组
    
    $.ajax({
        type: "get",
        url: "/index/Company/getCompanyById",
        data: {
            id: urlParamarr[1],

        },
        async: "true",
        dataType: "json",
        success: function (data) {

            if(language == "cn"){
                var sell_price = "出售价格";
                var corporate_income = "公司收入";
                var company_debt = "公司负债";
                var sell_reason = "出售原因";
                var established_on = "成立时间";
                var company_profile = "公司简介";
                var business_scope = "经营范围";
                var business_certificate = "营业证明";
                var industry_Certificate = "行业证书";
            }else{
                var sell_price = "Sell Price";
                var corporate_income = "CorporateIncome";
                var company_debt = "Company Debt";
                var sell_reason = "Sell Reason";
                var established_on = "Established On";
                var company_profile = "Company Profile";
                var business_scope = "Business Scope";
                var business_certificate = "Business Certificate";
                var industry_Certificate = "Industry_Certificate";
            }
            if (data.code == "2") {
                $('.details_box4_1').empty();
                if (data.data.license_url == '' | data.data.license_url == null) {
                    var license_url = '/static/images/11979734.png'
                } else {
                    var license_url = data.data.license_url
                }
               $(".details_box5_1").show()
               $(".details_box6_1").show()
                attache()
                var lihtml = $(`
                    <div class="details_box4_2">
    
                        <ul>
                            <li>
                                <p>${sell_price}:&ensp;${data.data.price} </p>
                            </li>
                            <li>
                                <p>${corporate_income}:&ensp;${data.data.income} </p>
                            </li>
                            <li>
                                <p>${company_debt}:&ensp;${data.data.debt}  </p>
                            </li>
                            <li>
                                <p>${sell_reason}:&ensp;${data.data.sell_reason} </p>
                            </li>
                            <li>
                                <p>${established_on}:&ensp;${data.data.life_year}年 ${data.data.life_month}月</p>
                            </li>
                            <li>
                                <div class="col-xs-12" style="padding: 0;">
                                    <div >
    
                                        <p>${company_profile}:&ensp;${data.data.descs} </p>
                                    </div>
                                    <div >
    
                                        <p></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-xs-12" style="padding: 0;">
                                    <div >
    
                                        <p>${business_scope}:&ensp;${data.data.scope} </p>
                                    </div>
                                    <div >
    
                                        <p></p>
                                    </div>
                                </div>
                            </li>
    
    
                            <li>
                                <p>${business_certificate}:</p>
                            </li>
                            <li  class="license_img">
                                <img src="${license_url}" alt="" style="width: 200px;height: 200px;">
                            </li>
                           
                            <li>
                                <p>${industry_Certificate}:</p>
                            </li>
                            <li class="certificate_img">
                                
                            </li>

                        </ul>
                        </div>
                            `)
                $('.details_box4_1').html(lihtml);
                if (data.data.certificate_url == '' | data.data.certificate_url == null) {


                    var lihtml2 = $(`<img src="/static/images/11979734.png" alt="" style="width: 200px;height: 200px; " >`);
                    $('.certificate_img').html(lihtml2);
                } else {
                    $.each(data.data.certificate_url, function (i, item) {
                        var lihtml2 = $(`<img src="${item}" alt="" style="width: 200px;height: 200px; " >`);
                        $('.certificate_img').append(lihtml2);
                    })
                }
            } else if (data.code == "1") {
                $('.buy').show();
                $('.buy_check').show();
                $('#cover').show();

                getPrice()
                $('.details_box4_1').empty();
                var lihtml = $(`
                    <div class="details_box4_2">
    
                        <img src="/static/images/pc/mask.png" alt="" class="img-responsive">
                        </div>
                            `)

                $('.details_box4_1').html(lihtml);
            } else {
                if(language == "cn"){

                    $('.details_box4_1').html('查询错误!');
                }else{
                    $('.details_box4_1').html('Query error!');

                }
                
            }


        }
    })
}

//获取价格

function getPrice() {


    $.ajax({
        type: "get",
        url: "/index/Order/getPrice",

        async: "true",
        dataType: "json",
        success: function (data) {

            if(language == "cn"){
                var msg1 = "购买查看";
                var a  = "一条";
                var quarter = "季度";
                var semester = "半年";
                var year = "一年";
                var msg2 = "选择您要购买的业务";
                var close = "关闭";
                
            }else{
                var msg1 = "Buy view";
                var a = "A strip of";
                var quarter = "Quarter";
                var semester = "Semester";
                var year = "Year";
                var msg2 = "Select the business you want to buy";
                var close = "close";
            }

             
                var lihtml = $(`
                        <div class="buy_1 col-xs-12 " style="">
                            <div class="buy_2 "  style="">
                                
                                <p class="close1" style="text-decoration: none;color: white;float: right;padding-right: 20px; cursor: pointer; padding-top:10px;" onclick="close1()">${close}</p>
                                <h4>${msg1}</h4>
                            </div>
                            <div class="buy_3 ">
                                <ul>
                                    <li class="one" style="">
                                        
                                            <a  href="javascript:void(0)" type="1" onclick="price(this.type)"  style="">
                                                
                                            <div class="buy_5">
                                                <span value="${data["3"].name}">${a}</span>
                                            </div>
                                            <div class="buy_6">
                                                <span class="price" value="${data["3"].value}">￥${data["3"].value}</span>
                                            </div>
                                            </a> 
                                       
                                        
                                    </li>
                                 
                                  
                                    
                                   <div class="vip_meal">
                                  
        
                                    <li>
                                        <a class="active" href="javascript:void(0)" type="2" onclick="price(this.type)">
                                            <div class="buy_5">
                                                <span value="${data["0"].name}">${quarter}</span>
                                            </div>
                                            <div class="buy_6">
                                                <span class="price" value="${data["0"].value}">￥${data["0"].value}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="javascript:void(0)" type="3" onclick="price(this.type)">
                                            <div class="buy_5">
                                                <span value="${data["1"].name}">${semester}</span>
                                            </div>
                                            <div class="buy_6">
                                                <span class="price" value="${data["1"].value}">￥${data["1"].value}</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="javascript:void(0)" type="4" onclick="price(this.type)">
                                            <div class="buy_5">
                                                <span value="${data["2"].name}" >${year}</span>
                                            </div>
                                            <div class="buy_6">
                                                <span class="price" value="${data["2"].value}">￥${data["2"].value}</span>
                                            </div>
                                        </a>
                                    </li>
                                    </div>
                                </ul>
                                
                               
                            </div>
                            
                            <div class="buy_4 col-xs-12">
                                <span>${msg2}</span><br><br> 
                                <a href="javascript:void(0)"  onclick="(buy())">${msg1}</a>                                     
                               
                            </div>
                            
                        </div>
                                `)
                         


            $('.buy_price').html(lihtml);


        }
    })
}

function close1() {
    $(".buy_price").hide();
    $('#cover').hide();
    // $(".buy_price").css("display","none");
    // $(".buy_price").empty();
}
function buy_check() {
    $(".buy_price").show();
    $("#cover").show();
}


//购买
function buy() {
    var type = ""
    var charge_type = $('.buy_3').find("a[class='active']").attr("type")
    if(charge_type < 2){

        type = "2";
        var company_id = $(".company_name").attr("value")
        var price = $('.one').find("span").eq(1).attr("value")
        // alert(company_id)
        var vip_charge_type = '';
    }else{
        type = "1";
        var company_id = '';
        var vip_charge_type = $('.vip_meal').find("a[class='active']").find("span").eq(0).attr("value")
        var price = $('.vip_meal').find("a[class='active']").find("span").eq(1).attr("value")
    }
    
    // if (type == "2") {
    //     var company_id = $(".company_name").attr("value")
    //     var price = $('.one').find("span").eq(1).attr("value")
    //     // alert(company_id)
    //     var vip_charge_type = '';
    // } else if (type == "1") {
    //     // alert(1)
    //     var company_id = '';
    //     var vip_charge_type = $('.vip_meal').find("a[class='active']").find("span").eq(0).attr("value")
    //     var price = $('.vip_meal').find("a[class='active']").find("span").eq(1).attr("value")

    // }
    var lihtml = $(`
            <form action="/index/Order/setOrder" method="post" >
                <input type="text" name="order_type" value="${type}" />
                <input type="hidden" name="company_id" value="${company_id}" />
                <input type="hidden" name="vip_charge_type" value="${vip_charge_type}" />
                <input type="hidden" name="price" value="${price}" />
                <input type="submit" id="sub" value=""/>
            </form>
            `)
    $(".form1").empty()
    $(".form1").html(lihtml)
    $("#sub").click();



}

//联系平台
function contact_platform() {
   
    var company_id = $("#company_name").attr("value");
    var name = $.trim($("#name").val());
    var phone = $.trim($("#phone").val());
    var email = $.trim($("#email").val());

    if(language == "cn"){
        var name_msg = "请填写您的名字"
        var phone_msg = "请填写您的电话"
        var phone_msg1 = "请输入正确的手机号码"
        var email_msg = "请填写您的邮箱"
        var email_msg1 = "请输入正确的邮箱"
        
    }else{
        var name_msg = "Please fill in your name"
        var phone_msg = "Please fill in your telephone number"
        var phone_msg1 = "Please enter the correct cell phone number"
        var email_msg = "Please fill in your email address"
        var email_msg1 = "Please enter the correct email address"
    };

    if(name == ""){
        success_prompt(name_msg,'alert-danger');
        // alert("请填写您的名字");
        return false;
    }
    if(phone == ""){
        success_prompt(phone_msg,'alert-danger');
        // alert("请填写您的电话");
        return false;
    }
    if(!(/^1[3456789]\d{9}$/.test(phone))){ 
        success_prompt(phone_msg1,'alert-danger');
        // alert("请输入正确的手机号码");  
        return false; 
    } 
    if(email == ""){
        success_prompt(email_msg,'alert-danger');
        // alert("请填写您的邮箱");
        return false;
    }
    if(!( /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(email))){ 
        success_prompt(email_msg1,'alert-danger');
        // alert("请输入正确的邮箱");  
        return false; 
    } 

    $.ajax({
        type: "POST",
        url: "/company/purchase_contact",
        data: { 
            company_id: company_id,
            name: name,
            phone: phone,
            email: email
         },
        async: "true",
        dataType: "json",
        success: function (data) {
           
           if(data.code == 1){
               alert(data.message)
           }else{

               alert(data.message)
           }
        }
    })
}

function attache(){
    $.ajax({
        type: "POST",
        url: "/common/Commissioner",
       
        async: "true",
        dataType: "json",
        success: function (data) {
            if(data.code == 1){
                $(".details_box6_0").empty();

                if(language == "cn"){
                    var msg = "24小时客服在线咨询";
                }else{
                    var msg = "24 hours customer service online consultation";
                    
                }
                var lihtml = $(`
            <div class="details_box6_2 col-xs-6 col-sm-3">
                <a  target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${data.data[0].qq}&site=qq&menu=yes">
                    <div class="details_box6_3">
                        <img src="/static/images/pc/icon03.png" alt="">
                    </div>
                    <div>
                        <p>${data.data[0].name}</p>
                        <p>${msg}</p>
                    </div>
                </a>
            </div>
            <div class="details_box6_2 col-xs-6 col-sm-3">
            <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${data.data[1].qq}&site=qq&menu=yes">
                <div class="details_box6_3">
                    <img src="/static/images/pc/icon03.png" alt="">
                </div>
                <div>
                    <p>${data.data[1].name}</p>
                    <p>${msg}</p>
                </div>
                </a>
            </div>
            <div class="details_box6_2 col-xs-6 col-sm-3">
            <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=${data.data[2].qq}&site=qq&menu=yes">
                <div class="details_box6_3">
                    <img src="/static/images/pc/icon03.png" alt="">
                </div>
                <div>
                    <p>${data.data[2].name}</p>
                    <p>${msg}</p>
                </div>
                </a>
            </div>

            <div class="details_box6_2 col-xs-6 col-sm-3">
                <div class="details_box6_3">
                    <img src="/static/images/pc/icon04.png" alt="">
                </div>
                <div>
                    <p>SKYPE客服</p>
                    <p>${msg}</p>
                </div>

            </div>
                `)
                $(".details_box6_0").append(lihtml);
           }else{

           }
        }
    })
}