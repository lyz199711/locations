<?php

return [
    'Name'        => '景点名称',
    'Classify_id' => '景点分类',
    'Coordinate'  => '景点坐标',
    'Region'      => '地区',
    'Create_time' => '创建时间',
    'Update_time' => '更新时间',
    'Delete_time' => '删除时间'
];
