<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Classify extends Backend
{
    
    /**
     * Classify模型对象
     * @var \app\admin\model\Classify
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Classify;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function add()
    {
        if ($this->request->isPost()){
            $params = $this->request->param('row/a');
            $name = $this->model->where('delete_time',null)->column('name');
            if (in_array($params['name'],$name)){
                $this->error('该分类已经存在');
            }
            $params['create_time'] = $params['update_time'] = time();
            try{
                Db::startTrans();
                $result = $this->model->allowField(true)->save($params);
            }
            catch (PDOException $PDOException){
                Db::rollback();
                $this->error($PDOException->getMessage());
            }
            catch (Exception $exception){
                Db::rollback();
                $this->error($exception->getMessage());
            }
            if ($result){
                Db::commit();
                $this->success('添加成功!');
            }else{
                Db::rollback();
                $this->error('添加失败!');
            }

        }
        return $this->fetch();
    }

}
