<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Collection;
use think\Db;
use think\Exception;
use think\exception\PDOException;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Locations extends Backend
{
    
    /**
     * Locations模型对象
     * @var \app\admin\model\Locations
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Locations;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
    public function add()
    {
        if ($this->request->isPost()){
            $params = $this->request->param('row/a');
            $name = $this->model->where('delete_time',null)->column('name');
            if (in_array($params['name'],$name)){
                $this->error('改景点已经存在');
            }
            $params['create_time'] = $params['update_time'] = time();
            try{
                Db::startTrans();
                $result = $this->model->allowField(true)->save($params);
            }
            catch (PDOException $PDOException){
                Db::rollback();
                $this->error($PDOException->getMessage());
            }
            catch (Exception $exception){
                Db::rollback();
                $this->error($exception->getMessage());
            }
            if ($result){
                Db::commit();
                $this->success('添加成功!');
            }else{
                Db::rollback();
                $this->error('添加失败!');
            }

        }
        return $this->fetch();
    }

    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where)
                ->where('delete_time',null)
                ->order($sort, $order)
                ->paginate($limit);
            $locations  = \collection($list->items())->toArray();
            foreach ($locations as $key => $value){
                $locations[$key]['classify_id'] = $this->getClassifyById($value['classify_id']);
                $locations[$key]['region'] = $this->getRegionById($value['region']);
            }
            $result = array("total" => $list->total(), "rows" => $locations);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * Notes:通过分类id获取分类名称
     */
    public function getClassifyById($id)
    {
        $model =new \app\admin\model\Classify();
        $classify = $model->where('id',$id)->where('delete_time',null)->value('name');
        return $classify;
    }

    /**
     * Notes:通过地区id获取地区名称
     */
    public function getRegionById($id)
    {
        $model = new \app\admin\model\Region();
        $region = $model->where('id',$id)->where('delete_time',null)->value('name');
        return $region;
    }
}
