<?php

namespace app\index\controller;

use app\admin\model\Classify;
use app\admin\model\Locations as locationModel;
use app\common\controller\Frontend;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        //获取初始景点
        $locationsList = locationModel::where('delete_time',null)->select();
        $locationsList = collection($locationsList)->toArray();
        //获取初始的景点类型
        $classifyList = Classify::where('delete_time',null)->select();
        $classifyList = collection($classifyList)->toArray();
        $this->assign('locationsList',$locationsList);
        $this->assign('classifyList',$classifyList);
        return $this->view->fetch();
    }

}
