<?php


namespace app\index\controller;


use think\Controller;
use app\admin\model\Locations as locationModel;
use app\admin\model\Classify;

define('PI',3.1415926535898);
define('EARTH_RADIUS',6378.137);

class Locations extends Controller
{
    /**
     * Notes:获取路线
     * $route 预设路线值 array
     * $distance 预设距离 int
     * $classify 预设下个景点的类别 float
     */
    public function getRoute($route = null, $distance = null, $classify =null)
    {
        if (empty($route)) {
            $this->error('请选择初始景点');
        }
        $newRoute = [];
        foreach ($route as $key1 => $value) {
            //获取最后一个景点id
            $value = explode(',', $value);
            $lastId = end($value);
            $lastLocation = locationModel::where('id', $lastId)->find();
            $coordinate = explode(',', $lastLocation['coordinate']);
            //加入条件获取下一个景点
            $where = [];
            if (!empty($classify)) {
                $where['classify_id'] = $classify;
            }
            $where['delete_time'] = null;
            $locations = locationModel::where($where)->select();
            $locationsArr = collection($locations)->toArray();
            $value = implode(',', $value);
            foreach ($locationsArr as $key2 => $value2) {
                if (empty($distance)) {
//                            $newRoute[] = array_push($value,$value2);
                    $newRoute[] = $value . ',' . $value2['id'];
                } else {
                    //计算距离
                    $coordinate2 = explode(',', $value2['coordinate']);
                    $juli = $this->getDistance($coordinate[0], $coordinate[1], $coordinate2[0], $coordinate2[1]);
                    if ($juli <= $distance) {
//                                $newRoute[] = array_push($value,$value2);
                        $newRoute[] = $value . ',' . $value2['id'];
                    }
                // }
            }
        }
        dump($newRoute);die;
        return $newRoute;
    }
    //计算2个坐标之间的距离
    public function getDistance($lat1, $lng1, $lat2, $lng2)
    {
//        define('PI',3.1415926535898);
//        define('EARTH_RADIUS',6378.137);
        $radLat1 = $lat1 * (PI / 180);
        $radLat2 = $lat2 * (PI / 180);

        $a = $radLat1 - $radLat2;
        $b = ($lng1 * (PI / 180)) - ($lng2 * (PI / 180));

        $s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));
        $s = $s * EARTH_RADIUS;
        $s = round($s * 10000) / 10000;
        return $s;
    }
}